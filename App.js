/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
// globals
import 'react-native-gesture-handler';
import React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

// react-navigation
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

//screens
import Jobs from './src/screens/Jobs';
import JobDetails from './src/screens/JobDetails';
import Academy from './src/screens/Academy';
import AiDesk from './src/screens/AiDesk';
import Insights from './src/screens/Insights';

// navigators
const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

// job stack nav
function JobStackScreen() {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Jobs" component={Jobs} />
      <Stack.Screen name="Job Detail" component={JobDetails} />
    </Stack.Navigator>
  );
}

// root bottom nav
export default function App() {
  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <Tab.Navigator
          tabBarOptions={{
            activeTintColor: '#1d192c',
            inactiveTintColor: '#98979d',
            labelStyle: {
              fontSize: 14,
            },
            style: {
              borderTopLeftRadius: 30,
              borderTopRightRadius: 30,
              borderColor: '#98979d',
              borderWidth: 1,
              paddingTop: 8,
            },
          }}>
          <Tab.Screen
            name="Jobs"
            component={JobStackScreen}
            options={{
              tabBarIcon: ({color}) => (
                <Icon name="briefcase-search" size={24} color={color} />
              ),
            }}
          />
          <Tab.Screen
            name="Academy"
            component={Academy}
            options={{
              tabBarIcon: ({color}) => (
                <Icon name="book-multiple" size={24} color={color} />
              ),
            }}
          />
          <Tab.Screen
            name="AI Desk"
            component={AiDesk}
            options={{
              tabBarIcon: ({color}) => (
                <Icon name="face-agent" size={24} color={color} />
              ),
            }}
          />
          <Tab.Screen
            name="Insights"
            component={Insights}
            options={{
              tabBarIcon: ({color}) => (
                <Icon name="filter-variant" size={24} color={color} />
              ),
            }}
          />
        </Tab.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
}
