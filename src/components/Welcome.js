// globals
import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

// image
import profile from '../assets/dp.jpeg';

// component
const Welcome = (props) => (
  <View style={styles.container}>
    <View style={styles.info}>
      <Text style={styles.helloText}>Hello,</Text>
      <Text style={styles.nameText}>Gayathri.</Text>
    </View>
    <View style={styles.profileWrapper}>
      <Image source={profile} style={styles.profile} />
      <View style={styles.activeDot} />
    </View>
  </View>
);
export default Welcome;

// styles
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  profileWrapper: {
    display: 'flex',
    flexDirection: 'row',
  },
  profile: {
    width: 50,
    height: 50,
    borderRadius: 15,
  },
  helloText: {
    fontSize: 18,
    color: '#989aac',
  },
  nameText: {
    fontSize: 22,
    fontWeight: 'bold',
    paddingTop: 4,
    color: '#1e1a2d',
  },
  activeDot: {
    width: 15,
    height: 15,
    borderRadius: 7.5,
    backgroundColor: '#ff7751',
    marginLeft: -10,
    marginTop: -5,
    borderWidth: 2,
    borderColor: '#fff',
  },
});
