import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Academy = () => (
  <View style={styles.container}>
    <Text>Academy</Text>
  </View>
);
export default Academy;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
